import {Component, Input} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NzMenuModule} from "ng-zorro-antd/menu";
import {RouterLink} from "@angular/router";


interface NavBarItem {
  title: string,
  icon: string,
  routerLink?: string[],
  children?: NavBarItem[]
}

@Component({
  selector: 'app-navbar',
  standalone: true,
  imports: [CommonModule, NzMenuModule, RouterLink],
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.less']
})

export class NavbarComponent {
  @Input() navBarItems: NavBarItem[] = [
    {
      title: "Test1",
      icon: "",
      children: [{
        title: "Test1Child1",
        icon: "",
        routerLink: ['/test1Child1']
      }]
    },
    {
      title: "Test2",
      icon: "",
      routerLink: ['/test2']
    },
    {
      title: "Test3",
      icon: "",
      routerLink: ['/test3']
    },
    {
      title: "Test4",
      icon: "",
      routerLink: ['/test4']
    }
  ]
  @Input() modeNavBar: 'inline' | 'horizontal' = 'inline';
  @Input() isCollapsed: boolean = false;

}
