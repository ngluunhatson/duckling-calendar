import {Component, Input} from '@angular/core';
import {CommonModule, NgOptimizedImage} from '@angular/common';
import {RouterOutlet} from "@angular/router";
import {NzIconModule} from 'ng-zorro-antd/icon';
import {NzButtonModule} from 'ng-zorro-antd/button';
import {NzImageModule} from "ng-zorro-antd/image";
import {NzDividerModule} from "ng-zorro-antd/divider";
import {NavbarComponent} from "../../components/navbar/navbar.component";

@Component({
  selector: 'app-default-layout',
  standalone: true,
  imports: [CommonModule, RouterOutlet, NzIconModule, NzButtonModule, NzImageModule, NgOptimizedImage, NzDividerModule, NavbarComponent],
  templateUrl: './default-layout.component.html',
  styleUrls: ['./default-layout.component.less']
})
export class DefaultLayoutComponent {
  @Input() pageTitle = "Duckling Calendar";
  isNavBarHidden = false;

  setIsNavbarHidden(value: boolean) {
    this.isNavBarHidden = value;
  }
}
