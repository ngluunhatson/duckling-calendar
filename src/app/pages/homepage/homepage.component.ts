import {Component} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DefaultLayoutComponent} from "../../layouts/default-layout/default-layout.component";

@Component({
  selector: 'app-homepage',
  standalone: true,
  imports: [CommonModule, DefaultLayoutComponent],
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.less']
})
export class HomepageComponent {

}
